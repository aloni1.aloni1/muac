import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './src/Home';
import { routes } from './src/const/routes';
import { Result } from './src/result';
import { ChooseMethod } from './src/chooseMethod';
import CameraMain from './src/Camera';

const { Screen, Navigator } = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Navigator
        screenOptions={{
          headerShown: false
        }}>
        <Screen name={routes.homeRoute} component={Home} />
        <Screen name={routes.resultRoute} component={Result} />
        <Screen name={routes.chooseMethodRoute} component={ChooseMethod} />
        <Screen name={routes.cameraRoute} component={CameraMain} />
      </Navigator>
    </NavigationContainer>
  );
};

export default App;
