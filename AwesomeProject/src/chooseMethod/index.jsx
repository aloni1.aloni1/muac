import * as React from 'react';
import { View, Dimensions } from 'react-native';
import { Surface, IconButton, MD2Colors } from 'react-native-paper';
import { routes } from '../const/routes';
import MeasuringTapeIcon from '../../assets/icon-measuring_tape.png'
import { GoBackButton } from '../components/goBackButton';

export const ChooseMethod = ({ navigation }) => {
    const screenWidth = Dimensions.get('window').width;

    return (
        <View>
            <Surface
                style={{
                    height: '100%',
                    width: '100%',
                    alignItems: 'center',
                    flexDirection: 'column',
                    justifyContent: 'space-evenly',
                    padding: 50,
                    backgroundColor: MD2Colors.white
                }}
            elevation={0}>
            <IconButton
                icon='camera'
                size={150}
                style={{
                    backgroundColor: MD2Colors.grey300,
                    borderRadius: screenWidth * 0.1
                }}
                onPress={() => navigation.navigate(routes.cameraRoute)}
            />
            <IconButton
                icon={MeasuringTapeIcon}
                size={150}
                style={{
                    backgroundColor: MD2Colors.yellow200,
                    borderRadius: screenWidth * 0.1
                }}
                onPress={() => console.log('Pressed')}
            />
            <GoBackButton navigation={navigation}/>
            </Surface>
        </View>
    );
};