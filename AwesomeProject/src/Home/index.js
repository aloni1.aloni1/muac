import * as React from 'react';
import { View } from 'react-native';
import { Surface, IconButton, MD2Colors } from 'react-native-paper';

import PregnantWomanIcon from '../../assets/icon-pregnant.png'
import BabyIcon from '../../assets/icon-baby.png'
import KidsIcon from '../../assets/icon-kids.png'
import { routes } from '../const/routes';

const Home = ({ navigation }) => {
  return (
    <View>
      <Surface
        style={{
          height: '100%',
          width: '100%',
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'space-evenly',
          padding: 50,
          backgroundColor: MD2Colors.white
        }}
        elevation={0}>
        <IconButton
          icon={PregnantWomanIcon}
          size={150}
          style={{
            backgroundColor: MD2Colors.purple100
          }}
          onPress={() => navigation.navigate(routes.chooseMethodRoute)}
        />
        <IconButton
          icon={BabyIcon}
          size={150}
          style={{
            backgroundColor: MD2Colors.blue100
          }}
          onPress={() => navigation.navigate(routes.chooseMethodRoute)}
        />
        <IconButton
          icon={KidsIcon}
          size={150}
          style={{
            backgroundColor: MD2Colors.green100
          }}
          // todo: remove it this is just an example on how to navigate to a result screen
          onPress={() => navigation.navigate(routes.resultRoute, { color: 'green'})}
        />
      </Surface>
    </View>
  );
};

export default Home;
