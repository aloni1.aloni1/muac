
import React from 'react';
import { View } from 'react-native';
import { GoBackButton } from '../components/goBackButton';

export const Result = ({ route, navigation }) => {
    const color = route.params.color;

    return (
        <View style={{backgroundColor: color,  flex: 1}}>
            <GoBackButton navigation={navigation}/>       
        </View>
    );
};