
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity, Dimensions } from 'react-native';

export const GoBackButton = ({ navigation }) => {
    const screenWidth = Dimensions.get('window').width;

    return (
        <TouchableOpacity style={{ position: 'absolute', left: 0, bottom: 0}} onPress={() => navigation.goBack()}>
            <Icon name="arrow-back" size={screenWidth * 0.2} color="black" />
        </TouchableOpacity>        
    );
};